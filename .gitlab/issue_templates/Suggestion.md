<!-- This template is the default template for making a proposal.-->

### Proposal

<!-- Use this section to explain what you want to add. -->
<!-- It can be helpful to add technical details, design proposals, and links to related epics or issues. -->

<!-- Add a label here matching the category of the suggestion, like ~enhancement or ~support -->

/label ~suggestion ~"workflow::backlog 📚"
