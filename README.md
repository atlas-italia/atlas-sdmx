
# Atlas SDMX - UI explorer for [SDMX](https://sdmx.org/) based services

[![pipeline status](https://gitlab.com/atlas-italia/atlas-sdmx/badges/main/pipeline.svg)](https://gitlab.com/atlas-italia/atlas-sdmx/-/commits/main)
[![coverage report](https://gitlab.com/atlas-italia/atlas-sdmx/badges/main/coverage.svg)](https://gitlab.com/atlas-italia/atlas-sdmx/-/commits/main)

---

[[_TOC_]]

## What is this

Atlas SDMX is a UI to help exploring SDMX-based services. It's FOSS and released under the
[low level of shame](https://about.gitlab.com/handbook/values/#low-level-of-shame) GitLab principle.

## Project Structure

The project consists of an `Express` server and a `Vue.js` client.

### Server

The server is nothing more than a simple proxy for SDMX based webservices. It is used to account for situations where the
last one does not allow `non-cors` requests.

_Note_: at the moment, the server defaults to the Italian National Institute of Statistics (ISTAT). More agencies
can (and will) be added in the future and made available. If you have a special need for a particular agency, feel free
to [open an Issue](https://gitlab.com/atlas-italia/atlas-sdmx/-/issues/new).

### Client

The client is a simple UI that allows to easily explore the data in the warehouse the statistics organization, filter it
and download a CSV of the resulting query.

The way the UI works is based on the guide elaborated and written
by [Associazione On Data](https://ondata.github.io/guida-api-istat/). An English (slightly more complicated) equivalent
can be found in this [Wiki](https://github.com/sdmx-twg/sdmx-rest/wiki).

## Dependencies

### Global

Both the server and the client depends on `Node.js` and its ecosystem. For a simple installation of `Node.js`, follow
the guide <a target="_blank" href="https://nodejs.org/en/">here</a>. This will install also `npm`, a `Node.js` package
manager.

The recommended `Node.js` version is the at least the <a target="_blank" href="https://nodejs.org/en/about/releases/">
Current</a>.

The project is written in TypeScript.

### Local

To install the project dependencies, simpy run `npm install`.

The frontend development environment depends on `webpack`. To know more about it, you can visit
<a target="_blank" href="https://webpack.js.org/">this page</a>.

## Run Atlas SDMX on local

To use Atlas SDMX simply run `npm start`. This will compile the necessary TypeScript and start the server.
If no config is provided (see later) the server will start listening at `http://localhost`.

(_Note_: for local development, it is recommended to define a post.)

## Local development

Developing locally is made easier with webpack development server.

You can then run the following commands:

1. `npm run client:serve` to start the webpack dev server, (with `hot-reload`)
1. `npm start` to start the local server which

Optionally, if you plan to work on the server as well, you can run `npm run server:watch` instead of 2), which will
start `nodemon` and keep watching for file changes.

If you run the server at a different domain / post, you need to specify the following variables before running webpack
dev server:

```shell
ATLAS_SDMX_HOST="<your-host>"
ATLAS_SDMX_PORT="<your-port>"
```

## Other commands

The `package.json` contains some commands to make local development easier.

Here's the list of most useful commands that you can run (for a full list, see [package.json](./package.json):

|      | Command                  | Description                                                                              |
| :--: | :--------------------| --------------------------------------------------------------------------------------       |
| 1    | `npm test`              | Run the tests suite.                                                                      |
| 2    | `npm test:coverage`     | Run the tests suite and display the  coverage report.                                     |
| 3    | `npm lint`              | Will check against faulty code style.                                                     |
| 4    | `npm lint:fix`          | Will check against faulty code style and fix them when possible.                          |


---

A project by <a href="https://www.instagram.com/atlas.sicilia/" target="_blank">Atlas Sicilia</a>.
