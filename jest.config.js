module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    cache: false,
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node', 'vue'],
    roots: ['<rootDir>/src'],
    testRegex: '(test|spec)\\.[jt]sx?$',
    testPathIgnorePatterns: ['/node_modules/'],
    coverageReporters: ['cobertura', 'text', 'json'],
    reporters: [
        'default',
        [
            'jest-junit',
            {
                outputDirectory: '<rootDir>/coverage',
                outputName: 'junit.xml',
            },
        ],
    ],
    coverageThreshold: {
        global: {
            branches: 0,
            functions: 30,
            lines: 40,
            statements: 45,
        },
    },
    collectCoverage: true,
    collectCoverageFrom: [
        './src/**/*.{ts,tsx}',
        '!./src/config/**/*.ts',
        '!**/node_modules/**',
        '!**/*.d.ts',
    ],
    coverageDirectory: '<rootDir>/coverage',
    verbose: true,
};
