import { request } from './helpers';
import { Package } from '../config/package';
import { LocalizedName } from '../typings/structuralMetadata';
import { Maybe } from '../typings';

interface CodeList {
    data: Record<string, any>;
}

export interface Code {
    id: string;
    locale: LocalizedName;
}

export interface Dimension {
    codes: Code[];
    id: string;
    locale: LocalizedName;
    name: string;
    position: number;
}

interface LocalRepresentation {
    enumeration: string;
}

export interface DimensionItem {
    conceptIdentity: string;
    id: string;
    localRepresentation?: LocalRepresentation;
    name: Maybe<string>;
    position: number;
    type?: string;
    urn?: string;
}

export const getCodeList = ({
    id,
    name,
    position,
}: DimensionItem): Promise<Dimension> =>
    request<CodeList>(Package.CODE_LIST, { resource: name as string }).then(
        ({ data }) => {
            const {
                codelists: [codeLists],
            } = data;

            const codes: Code[] = codeLists.codes.map(code => {
                return {
                    id: code.id,
                    locale: code.name,
                };
            });

            return {
                codes,
                id,
                locale: codeLists.name,
                name: codeLists.id,
                position,
            };
        }
    );
