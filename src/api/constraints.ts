import { DataFlowItem } from './index';
import { Package } from '../config/package';
import { request } from './helpers';
import {
    AvailableConstraintResponse,
    Constraint,
} from '../services/sdmx_proxy/sdmx/metadata/parsers/availableConstraint';

export type ConstraintsRecord = Record<
    Constraint['id'],
    Constraint['available']
>;

export const getAvailableConstraints = (
    id: DataFlowItem['id']
): Promise<ConstraintsRecord> =>
    request<AvailableConstraintResponse>(Package.AVAILABLE_CONSTRAINT, {
        resource: id,
    }).then(({ data }) => {
        const [allConstraints] = data;
        return allConstraints.constraints.reduce(
            (acc, constraint) => ({
                ...acc,
                [constraint.id]: constraint.available,
            }),
            {}
        );
    });
