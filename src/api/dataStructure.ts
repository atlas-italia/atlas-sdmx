import { Package } from '../config/package';
import { Meta, ResourceDescription } from '../typings/structuralMetadata';
import { DimensionItem } from './codeList';
import { getResourceName, request } from './helpers';

interface DataStructureComponents {
    dimensionList: DimensionsList;
}

interface Data {
    dataStructures: DataStructureItem[];
}

interface DimensionsList {
    dimensions: DimensionItem[];
}

export interface DataStructure {
    meta: Meta;
    data: Data;
}

export interface DataStructureItem extends ResourceDescription {
    dataStructureComponents: DataStructureComponents;
}

const amendDimensions = ({
    dimensionList,
}: DataStructureComponents): DataStructureComponents => ({
    dimensionList: {
        dimensions: dimensionList?.dimensions.map(dimension => {
            const name = getResourceName(
                dimension.localRepresentation?.enumeration || ''
            );
            return {
                ...dimension,
                name,
            };
        }),
    },
});

export const getDataStructure = (
    dataFlowResource: string
): Promise<DataStructureItem[]> =>
    request<DataStructure>(Package.DATA_STRUCTURE, {
        resource: dataFlowResource,
    }).then(({ data }) =>
        data.dataStructures.map(ds => {
            return {
                ...ds,
                dataStructureComponents: amendDimensions(
                    ds.dataStructureComponents
                ),
            };
        })
    );
