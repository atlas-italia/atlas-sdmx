import { Package } from '../config/package';
import { ExplorerRequestOptions } from './';
import { Maybe } from '../typings';
import { match, nth, pipe } from 'ramda';

const HOST = process.env.ATLAS_SDMX_HOST || '';
const PORT = process.env.ATLAS_SDMX_PORT
    ? `:${process.env.ATLAS_SDMX_PORT}`
    : '';

export const getResourceName: (p: string) => Maybe<string> = pipe(
    match(/[A-B0-9]:(\w*)\(/),
    nth(1)
);

export const buildUrl = (path: string): string => `${HOST}${PORT}${path}`;

export const request = <T extends unknown>(
    type: Package,
    options?: ExplorerRequestOptions
): Promise<T> => {
    const opt = Object.values(options || {})
        .filter(Boolean)
        .join('/');
    return fetch(buildUrl(`/${type}/${opt}`)).then(res => res.json());
};
