import { identity, mapObjIndexed, memoizeWith, propEq } from 'ramda';
import { Package } from '../config/package';
import { AgencyID } from '../config/agency';
import { LocalizedName, ResourceCommon } from '../typings/structuralMetadata';
import { buildUrl, request } from './helpers';
import { getDataStructure } from './dataStructure';
import { ConstraintsRecord, getAvailableConstraints } from './constraints';
import { Code, Dimension, getCodeList } from './codeList';

type ConstraintsRequest<T> = (
    d: Pick<DataFlowItem, 'id' | 'structure'>
) => Promise<T>;

export interface ExplorerRequestOptions {
    agencyID?: AgencyID;
    resource?: string;
}

export interface DataFlow {
    data: {
        dataflows: DataFlowItem[];
    };
}

export interface DataFlowItem extends ResourceCommon {
    agencyID: AgencyID;
    isFinal: boolean;
    name: LocalizedName;
    structure: string;
    version?: string;
}

export interface Filter
    extends Pick<Dimension, 'id' | 'locale' | 'name' | 'position'> {
    options: Code[];
}

const getConstraints = memoizeWith<
    (id: DataFlowItem['id']) => Promise<ConstraintsRecord>
>(identity, id => getAvailableConstraints(id));

export const getAllDataFlows = (): Promise<DataFlowItem[]> =>
    request<DataFlow>(Package.DATA_FLOW).then(({ data }) => data.dataflows);

export const getAvailableFilters: ConstraintsRequest<Filter[]> = async ({
    id,
    structure,
}) => {
    const [dataStructure] = await getDataStructure(structure);
    const constraints = await getAvailableConstraints(id);
    const dimensions = await Promise.all(
        dataStructure.dataStructureComponents.dimensionList.dimensions.map(
            getCodeList
        )
    );
    return Object.values(
        mapObjIndexed<string[], Filter>((constraint, key) => {
            const dimension = dimensions.find(propEq('id', key)) as Dimension;
            const { id, locale, name, position } = dimension;
            const isConstraintAvailable = ({ id }: Code) =>
                constraint.includes(id);
            const options = dimension.codes.filter(isConstraintAvailable);
            return {
                id,
                locale,
                name,
                options,
                position,
            };
        }, constraints)
    );
};

export const getConstraintsNumber: ConstraintsRequest<number> = async ({
    id,
}): Promise<number> => {
    const constraints = await getConstraints(id);
    return Object.keys(constraints).length;
};

export const getCsvFileUrl = (...args) =>
    buildUrl(`/${['csv', ...args].filter(Boolean).join('/')}`);
