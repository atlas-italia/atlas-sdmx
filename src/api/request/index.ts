import * as request from 'request';

export type RequestService = <T extends any>(
    url: string,
    options?: any
) => Promise<T>;

export const requestService = <T extends any>(
    url: string,
    options = {}
): Promise<T> =>
    new Promise((resolve, reject) => {
        const opt = {
            url,
            method: 'GET',
            ...options,
        };
        request(opt, (err, res, body) => {
            if (err) reject(err);
            resolve(body);
        });
    });
