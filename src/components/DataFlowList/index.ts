import { defineComponent, inject } from 'vue';
import { DataFlowItem, getAllDataFlows } from '../../api';
import { getResourceName } from '../../api/helpers';
import DataFlowListItem from '../DataFlowListItem.vue';
import { Locale } from '../../typings/structuralMetadata';

interface ListItem extends DataFlowItem {
    localizedName: string | undefined;
}

export default defineComponent({
    name: 'DataFlowList',
    components: { DataFlowListItem },
    emits: ['update:modelValue'],
    data() {
        return {
            dataFlowSearchTerm: '' as string,
            dataFlows: [] as DataFlowItem[],
            selectedDataFlow: {} as DataFlowItem,
        };
    },
    props: {
        loading: {
            type: Boolean,
            required: false,
            default: false,
        },
    },
    computed: {
        dataFlowListClasses(): string {
            return this.loading ? 'md:pointer-events-none' : '';
        },
        list(): ListItem[] {
            return this.dataFlows.reduce(
                (all, flow) =>
                    this.shouldFilterItem(flow.name)
                        ? [
                              ...all,
                              {
                                  ...flow,
                                  localizedName: flow?.name[this.currentLocale],
                                  structure: getResourceName(
                                      flow.structure || ''
                                  ),
                              } as ListItem,
                          ]
                        : all,
                [] as ListItem[]
            );
        },
        title(): string {
            return `Dataflow (${this.list.length}/${this.dataFlows.length})`;
        },
    },
    setup() {
        const currentLocale = inject('currentLocale') as Locale;
        return { currentLocale };
    },
    methods: {
        isCurrentDataFlow(dataFlow) {
            return dataFlow.id === this.selectedDataFlow.id;
        },
        selectDataFlow(dataFlow) {
            this.selectedDataFlow = dataFlow;
            this.$emit('update:modelValue', dataFlow);
        },
        shouldFilterItem(dataFlowName) {
            return (
                !dataFlowName ||
                dataFlowName[this.currentLocale]
                    .toLowerCase()
                    .includes(this.dataFlowSearchTerm.toLowerCase())
            );
        },
    },
    mounted(): void {
        getAllDataFlows().then(dataFlows => {
            this.dataFlows = dataFlows.sort(
                (prev, next) =>
                    prev?.name[this.currentLocale]?.localeCompare(
                        next?.name[this.currentLocale] as string
                    ) as number
            );
        });
    },
});
