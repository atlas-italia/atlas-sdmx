import { defineComponent, PropType } from 'vue';
import { DataFlowItem } from '../../api';

interface DataFLow extends DataFlowItem {
    localizedName: string | undefined;
}

export default defineComponent({
    name: 'DataFlowListItem',
    emits: ['update'],
    props: {
        dataFlow: {
            type: Object as PropType<DataFLow>,
            required: true,
        },
        selected: {
            type: Boolean,
            required: false,
            default: false,
        },
    },
    inject: {
        currentLocale: {},
    },
    computed: {
        itemClass(): string {
            return this.selected ? 'bg-pink-atlas hover:bg-pink-atlas' : '';
        },
    },
    methods: {
        selectDataFlow() {
            this.$emit('update', this.dataFlow);
        },
    },
});
