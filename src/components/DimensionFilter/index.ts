import { defineComponent, inject, PropType, ref } from 'vue';
import { contains, pipe, prop, toLower } from 'ramda';
import { Locale } from '../../typings/structuralMetadata';
import { Code } from '../../api/codeList';
import { Filter } from '../../api';
import UIArrow from '../UI/Icons/Arrow.vue';
import { Maybe } from '../../typings';

interface LocalizedCode extends Omit<Code, 'locale'> {
    localizedName: string;
}

export const matchesSearchTerm = (searchTerm: string) =>
    pipe<LocalizedCode, string, string, boolean>(
        prop('localizedName'),
        toLower,
        contains(searchTerm.toLowerCase())
    );

export default defineComponent({
    name: 'DimensionFilter',
    components: {
        UIArrow,
    },
    emits: ['update'],
    computed: {
        currentSelectionClass(): string {
            return this.selectedOption?.localizedName ? '' : 'text-gray-300';
        },
        currentSelectionValue(): string {
            return this.selectedOption.localizedName || 'Seleziona un valore';
        },
        dropdownClass(): string {
            return this.isOpen ? 'border-pink-atlas' : '';
        },
        filteredOptions(): LocalizedCode[] {
            if (this.optionSearchTerm.length) {
                return this.localizedOptions.filter(
                    matchesSearchTerm(this.optionSearchTerm)
                );
            }
            return this.localizedOptions;
        },
        localizedName(): string {
            return this.locale[this.currentLocale];
        },
        localizedOptions(): LocalizedCode[] {
            return this.options
                .map(option => {
                    return {
                        ...option,
                        localizedName: option.locale[this.currentLocale],
                    };
                })
                .sort(
                    (prev, next) =>
                        prev.localizedName?.localeCompare(
                            next.localizedName as string
                        ) as number
                );
        },
        shouldShowSearch(): boolean {
            return this.options.length > 6;
        },
    },
    setup() {
        const currentLocale = inject('currentLocale') as Locale;
        const isOpen = ref<boolean>(false);
        const optionSearchTerm = ref<string>('');
        const search = ref<Maybe<HTMLInputElement>>(null);
        const selectedOption = ref<LocalizedCode>({} as LocalizedCode);

        return {
            currentLocale,
            isOpen,
            optionSearchTerm,
            search,
            selectedOption,
        };
    },
    methods: {
        closeList() {
            this.isOpen = false;
        },
        getOptionClass(id) {
            return this.selectedOption.id === id
                ? 'bg-pink-atlas hover:bg-pink-atlas'
                : '';
        },
        openList() {
            this.isOpen = !this.isOpen;
        },
        searchRef(el: HTMLInputElement): void {
            this.search = el;
        },
        selectOption(option) {
            this.selectedOption =
                this.selectedOption.id === option.id ? {} : option;
            this.$emit('update', this.selectedOption, this.id);
            this.optionSearchTerm = '';
            this.isOpen = false;
        },
    },
    props: {
        id: {
            type: String as PropType<Filter['id']>,
            required: true,
        },
        locale: {
            type: Object as PropType<Filter['locale']>,
            required: true,
        },
        name: {
            type: String as PropType<Filter['name']>,
            required: true,
        },
        options: {
            type: Array as PropType<Code[]>,
            required: true,
        },
        position: {
            type: Number as PropType<Filter['position']>,
            required: true,
        },
    },
});
