import { generateUrlFromFilters } from './filters';

describe('Filters', () => {
    describe('generateUrlFromFilters', () => {
        it.each([
            [['.', '.', '.', '.'], '....'],
            [['A', '.', '.', '.'], 'A...'],
            [['.', 'A', '.', '.'], '.A..'],
            [['.', '.', 'A', '.'], '..A.'],
            [['.', '.', '.', 'A'], '...A'],
            [['A', 'B', '.', '.'], 'A.B..'],
            [['A', '.', 'B', '.'], 'A..B.'],
            [['A', '.', '.', 'B'], 'A...B'],
            [['A', 'B', '.', 'C'], 'A.B..C'],
            [['A', '.', 'B', 'C'], 'A..B.C'],
        ])(
            'correctly generates query filters from %s to %s',
            (filters, result) => {
                expect(generateUrlFromFilters(filters)).toBe(result);
            }
        );
    });
});
