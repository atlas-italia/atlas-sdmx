import { join } from 'ramda';

export const DEFAULT_FILTER_FOR_URL_VALUE = '.';

export const generateUrlFromFilters = (filters: string[]): string =>
    join(
        '',
        filters.reduce((acc, current, currentIndex, values) => {
            const isNextValueSet =
                values[currentIndex + 1] &&
                values[currentIndex + 1] !== DEFAULT_FILTER_FOR_URL_VALUE;

            const isTherePrevNonDefaultValue = acc.some(
                v => v !== DEFAULT_FILTER_FOR_URL_VALUE
            );

            if (current !== DEFAULT_FILTER_FOR_URL_VALUE && isNextValueSet) {
                return [...acc, current, DEFAULT_FILTER_FOR_URL_VALUE];
            }

            if (
                current === DEFAULT_FILTER_FOR_URL_VALUE &&
                isNextValueSet &&
                isTherePrevNonDefaultValue
            ) {
                return [...acc, DEFAULT_FILTER_FOR_URL_VALUE, current];
            }

            return [...acc, current];
        }, [] as string[])
    );
