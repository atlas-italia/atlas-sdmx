import { defineComponent, provide, ref } from 'vue';
import { equals, propEq } from 'ramda';
import DataFlowList from '../DataFlowList.vue';
import DimensionFilter from '../DimensionFilter.vue';
import UIButton from '../UI/Button.vue';
import UILoader from '../UI/Loader.vue';
import DocumentSearch from '../UI/Icons/DocumentSearch.vue';
import { AgencyID } from '../../config/agency';
import {
    DEFAULT_FILTER_FOR_URL_VALUE,
    generateUrlFromFilters,
} from './filters';
import { Locale } from '../../typings/structuralMetadata';
import {
    DataFlowItem,
    Filter,
    getAvailableFilters,
    getCsvFileUrl,
} from '../../api';
import { Code } from '../../api/codeList';
import { Maybe } from '../../typings';

export default defineComponent({
    name: 'SDMXExplorerApp',
    components: {
        DataFlowList,
        DimensionFilter,
        DocumentSearch,
        UIButton,
        UILoader,
    },
    computed: {
        hasFilters(): boolean {
            return !!this.availableFilters.length;
        },
        dataFlowListClass(): Record<string, boolean> {
            return {
                'md:cursor-wait': this.isLoadingFilters,
                'cursor-pointer': !this.isLoadingFilters,
                'transform -translate-x-full': !this.shouldShowDrawer,
            };
        },
        urlFilters(): string {
            const filters = generateUrlFromFilters(this.selectedFilters);
            return filters ? `${filters}/` : '';
        },
        urlForDataDownload(): string {
            return getCsvFileUrl(this.selectedDataFlow.id, this.urlFilters);
        },
        selectedDataFlowName(): string {
            return this.selectedDataFlow?.id
                ? this.selectedDataFlow?.name[this.currentLocale]
                : '';
        },
    },
    methods: {
        copyText(): void {
            const { value } = this.urlForData as HTMLInputElement;
            navigator.permissions
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                .query({ name: 'clipboard-write' })
                .then(result => {
                    if (result.state == 'granted' || result.state == 'prompt') {
                        navigator.clipboard
                            .writeText(value)
                            .catch(error => console.error(error))
                            .finally(() =>
                                this.displayNotification(
                                    'URL copiato correttamente'
                                )
                            );
                    }
                });
        },
        displayNotification(message: string): void {
            this.notificationText = message;
            setTimeout(() => (this.notificationText = ''), 2000);
        },
        getFilters(): void {
            this.availableFilters = [];
            this.isLoadingFilters = true;
            getAvailableFilters(this.selectedDataFlow)
                .then(result => {
                    this.availableFilters = result;
                    this.shouldShowDrawer = false;
                })
                .finally(() => (this.isLoadingFilters = false));
        },
        openDrawer() {
            this.shouldShowDrawer = true;
        },
        setFilter(filterValue: Code, id): void {
            const filter = this.availableFilters.find(
                propEq('id', id)
            ) as Filter;
            this.selectedFilters[filter.position] =
                filterValue.id || DEFAULT_FILTER_FOR_URL_VALUE;
            this.selectedFiltersValue[filter.position] = filterValue.locale
                ? filterValue.locale[this.currentLocale]
                : '';
        },
        urlForDataRef(el: HTMLInputElement): void {
            this.urlForData = el;
        },
    },
    setup() {
        const availableFilters = ref<Filter[]>([]);
        const currentLocale = ref<Locale>(Locale.IT);
        const isLoadingFilters = ref<boolean>(false);
        const notificationText = ref<string>('');
        const selectedAgency = ref<AgencyID>(AgencyID.ITALY);
        const selectedDataFlow = ref<DataFlowItem>({} as DataFlowItem);
        const selectedFilters = ref<string[]>([]);
        const selectedFiltersValue = ref<string[]>([]);
        const shouldShowDrawer = ref<boolean>(true);
        const urlForData = ref<Maybe<HTMLInputElement>>(null);
        provide('currentLocale', currentLocale);

        return {
            availableFilters,
            currentLocale,
            isLoadingFilters,
            notificationText,
            selectedAgency,
            selectedDataFlow,
            selectedFilters,
            selectedFiltersValue,
            shouldShowDrawer,
            urlForData,
        };
    },
    watch: {
        availableFilters(): void {
            this.selectedFilters = this.availableFilters.reduce(
                (acc, filter) => {
                    acc[filter.position] = DEFAULT_FILTER_FOR_URL_VALUE;
                    return acc;
                },
                [] as string[]
            );
        },
        selectedDataFlow(newValue, oldValue): void {
            if (equals(newValue, oldValue)) {
                return;
            }
            this.getFilters();
        },
    },
});
