import { defineComponent } from 'vue';

const DEFAULT_CLASSES =
    'bg-blue-atlas-light border-2 border-blue-atlas-light h-12 outline-none p-4 pt-3 pr-9 rounded-lg text-center text-white focus:outline-none hover:bg-blue-500';

export default defineComponent({
    name: 'UIButton',
    emits: ['click'],
    props: {
        href: {
            type: String,
            required: false,
            default: null,
        },
        label: {
            type: String,
            required: true,
        },
        title: {
            type: String,
            required: false,
            default: vm => vm.label,
        },
    },
    methods: {
        onClick(): void {
            this.$emit('click');
        },
    },
    setup() {
        const classes = DEFAULT_CLASSES;
        return {
            classes,
        };
    },
});
