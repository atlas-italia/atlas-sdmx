import { defineComponent } from 'vue';

const DEFAULT_BACKDROP_CLASSES =
    'bg-blue-atlas-darker bg-opacity-95 flex flex-col items-center justify-center fixed top-0 left-0 right-0 bottom-0 h-screen z-50 overflow-hidden pointer-none w-full';

export default defineComponent({
    name: 'UILoader',
    setup() {
        const backdropClasses = DEFAULT_BACKDROP_CLASSES;
        return { backdropClasses };
    },
});
