export enum Agency {
    ISTAT = 'ISTAT',
}

export enum AgencyID {
    NONE = '',
    ITALY = 'IT1',
    SDMX = 'SDMX',
}
