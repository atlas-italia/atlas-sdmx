export enum ApiVersion {
    v102 = 'v1.0.2',
    LATEST = 'v1.4.0',
}
