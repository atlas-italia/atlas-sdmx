import { Agency, AgencyID } from './agency';
import { AgencyConfig } from '../services/sdmx_proxy/sdmxProxyAbstract';

const istat = {
    agencyID: AgencyID.ITALY,
    id: 'IT1',
    name: Agency.ISTAT,
    restUrl: 'http://sdmx.istat.it/SDMXWS/rest',
};

export const agencyConfig: Record<Agency, AgencyConfig | null> = {
    [Agency.ISTAT]: istat,
};
