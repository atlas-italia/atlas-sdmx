export enum Package {
    AVAILABLE_CONSTRAINT = 'availableconstraint',
    CODE = 'code',
    CODE_LIST = 'codelist',
    CONCEPT_SCHEME = 'conceptscheme',
    DATA_FLOW = 'dataflow',
    DATA_STRUCTURE = 'datastructure',
    DIMENSION = 'dimension',
    DIMENSION_DESCRIPTOR = 'dimensiondescriptor',
}
