import * as express from 'express';
import { Agency } from '../config/agency';
import { Package } from '../config/package';
import { agencyConfig } from '../config/italy';
import { AgencyConfig } from '../services/sdmx_proxy/sdmxProxyAbstract';
import { createService } from '../services/sdmx_proxy/sdmxProxyFactory';
import {
    availableConstraintMiddleware,
    codeListMiddleware,
    cvsExportMiddleWare,
    dataFlowMiddleware,
    dataStructureMiddleware,
} from '../middlwares';

const app = express();
const istat = createService(Agency.ISTAT);

app.get(`/${Package.DATA_FLOW}/`, dataFlowMiddleware(istat))
    .get(`/${Package.DATA_STRUCTURE}/:resource`, dataStructureMiddleware(istat))
    .get(
        `/${Package.AVAILABLE_CONSTRAINT}/:resource`,
        availableConstraintMiddleware(istat)
    )
    .get(`/${Package.CODE_LIST}/:resource`, codeListMiddleware(istat))
    .get(
        `/csv/:dataflow/:filters*?`,
        cvsExportMiddleWare(agencyConfig[Agency.ISTAT] as AgencyConfig)
    );

module.exports = { app };
