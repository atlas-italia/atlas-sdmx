import { createApp } from 'vue';
import SDMXExplorerApp from './components/SDMXplorerApp.vue';
import { ClickOutsideDirective } from './directives/clickOutside';

const rootAppId = '#app';
document.addEventListener('DOMContentLoaded', () =>
    createApp({
        components: {
            SDMXExplorerApp,
        },
        template: `
      <SDMXExplorerApp />
    `,
    })
        .directive('click-outside', ClickOutsideDirective)
        .mount(rootAppId)
);
