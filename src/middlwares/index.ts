import {
    AgencyConfig,
    SDMXService,
} from '../services/sdmx_proxy/sdmxProxyAbstract';
import { Request, Response } from 'express';
import { responseWrapper } from './utils';
import {
    DataResponseFormat,
    MetaDataResponse,
} from '../typings/structuralMetadata';
import { requestService } from '../api/request';

export const dataFlowMiddleware = (agency: SDMXService) => (
    _,
    res: Response
): Promise<void> => {
    return responseWrapper<MetaDataResponse>(agency.getDataFlow(), res);
};

export const dataStructureMiddleware = (agency: SDMXService) => (
    req: Request,
    res: Response
): Promise<void> => {
    const { resource } = req.params;
    return responseWrapper<MetaDataResponse>(
        agency.getDataStructure({
            resourceID: resource,
        }),
        res
    );
};

export const availableConstraintMiddleware = (agency: SDMXService) => (
    req: Request,
    res: Response
): Promise<void> => {
    const { resource } = req.params;
    return responseWrapper<MetaDataResponse>(
        agency.getAvailableConstraint({
            resourceID: resource,
        }),
        res
    );
};

export const codeListMiddleware = (agency: SDMXService) => (
    req: Request,
    res: Response
): Promise<void> => {
    const { resource } = req.params;
    return responseWrapper<MetaDataResponse>(
        agency.getCodeList({
            resourceID: resource,
        }),
        res
    );
};

export const cvsExportMiddleWare = (agencyConfig: AgencyConfig) => (
    req: Request,
    res: Response
) => {
    const { agencyID, name, restUrl } = agencyConfig;
    const { dataflow, filters } = req.params;
    const url = `${restUrl}/data/${dataflow}/${filters}/`;
    const fileName = `data_${agencyID}_${name}_${dataflow}_${filters}_${Date.now().toString()}.csv`;
    res.setHeader('Content-disposition', `attachment; filename=${fileName}`);
    res.setHeader('Content-type', 'text/csv; charset=utf-8');
    const requestHeaders = {
        Accept: DataResponseFormat.SDMX_CSV,
    };
    requestService(url, { encoding: null, headers: requestHeaders }).then(
        file => {
            res.send(Buffer.from(file as string));
        }
    );
};
