import { Response } from 'express';

export const processFailingResponse = (res: Response) => (
    error: string
): void => {
    res.send(error);
};

export const processSuccessfulResponse = (res: Response) => (
    result: unknown
): void => {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'GET,');
    res.status(200).send(result);
};

export const responseWrapper = <T>(
    result: Promise<T>,
    res: Response
): Promise<void> =>
    result
        .then(processSuccessfulResponse(res))
        .catch(processFailingResponse(res));
