import { identity } from 'ramda';
import { URL } from 'url';
import {
    DataRequest,
    DataResource,
    MetadataQuery,
    MetaDataResponse,
} from '../../typings/structuralMetadata';
import { Agency, AgencyID } from '../../config/agency';
import { AbstractStatProxyFactory, AgencyConfig } from './sdmxProxyAbstract';
import { Package } from '../../config/package';
import { CodeListResponse } from './sdmx/metadata/parsers/codeList';
import { agencyConfig } from '../../config/italy';

export class IstatProxyStatService extends AbstractStatProxyFactory {
    public getData(requestParams: DataRequest): Promise<MetaDataResponse> {
        const {
            flowRef,
            key,
            queryParams,
            resource = DataResource.DATA,
        } = requestParams;
        const request = [flowRef, key.join('')].filter(Boolean).join('/');
        const url = new URL(`${this.config.restUrl}/${resource}/${request}`);
        Object.keys(queryParams || {}).forEach(
            (key: string) =>
                requestParams[key] &&
                url.searchParams.append(key, requestParams[key])
        );
        return Promise.resolve<MetaDataResponse>({} as MetaDataResponse);
    }

    getAvailableConstraint(params: MetadataQuery): Promise<any> {
        const resource = Package.AVAILABLE_CONSTRAINT;
        return this.requestMetaDataFromXMLFormat<CodeListResponse>({
            ...params,
            resource,
            agencyID: AgencyID.NONE,
        });
    }

    getCodeList(params: MetadataQuery): Promise<CodeListResponse> {
        const resource = Package.CODE_LIST;
        return this.requestMetaDataFromXMLFormat<CodeListResponse>({
            ...params,
            resource,
        });
    }

    getDataFlow(params: MetadataQuery = {}): Promise<MetaDataResponse> {
        const resource = Package.DATA_FLOW;
        return this.requestMetaDataFromXMLFormat({
            ...params,
            resource,
        })
            .then(identity)
            .catch(
                error =>
                    (({
                        error,
                    } as unknown) as MetaDataResponse)
            );
    }

    getDataStructure(params: MetadataQuery): Promise<MetaDataResponse> {
        const resource = Package.DATA_STRUCTURE;
        return this.requestMetaDataFromXMLFormat({
            ...params,
            resource,
        });
    }

    protected getConfig(): AgencyConfig {
        return agencyConfig[Agency.ISTAT] as AgencyConfig;
    }
}
