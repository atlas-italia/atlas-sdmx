import {
    Meta,
    ResourceDescription,
} from '../../../../../typings/structuralMetadata';
import { getMeta } from '../../../../../utils/parser';
import { AgencyID } from '../../../../../config/agency';
import { Package } from '../../../../../config/package';

export interface AvailableConstraints
    extends Omit<ResourceDescription, 'isFinal'> {
    attachment: ConstraintAttachment;
    constraints: Constraint[];
}

export interface Constraint {
    id: string;
    available: string[];
}

interface ConstraintAttachment {
    id: string;
    agencyID: AgencyID;
    resourcePackage: Package;
}

export interface AvailableConstraintResponse {
    meta: Meta;
    data: AvailableConstraints[];
}

export const constraintParser = async (
    xml: Promise<any>
): Promise<AvailableConstraintResponse> => {
    const dataMessage = await xml;
    const dataStructure =
        dataMessage['message:Structure']['message:Structures'][0][
            'structure:Constraints'
        ][0]['structure:ContentConstraint'][0];
    const meta = getMeta(dataMessage);
    const { agencyID, id, version } = dataStructure.$;
    const attachment =
        dataStructure['structure:ConstraintAttachment'][0][
            'structure:Dataflow'
        ][0]['Ref'][0].$;
    const constraints = dataStructure['structure:CubeRegion'][0][
        'common:KeyValue'
    ].map(({ $: { id }, ['common:Value']: available }) => ({ id, available }));

    const data = [
        {
            agencyID,
            attachment: {
                agencyID: attachment.agencyID,
                id: attachment.id,
                resourcePackage: attachment.package,
            },
            constraints,
            id,
            version,
        },
    ];

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return { meta, data };
};
