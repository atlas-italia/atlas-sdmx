import {
    LocalizedName,
    Meta,
    Relation,
    ResourceCommon,
    ResourceDescription,
} from '../../../../../typings/structuralMetadata';
import { getLocalizedName, getMeta } from '../../../../../utils/parser';
import { stringToBool } from '../../../../../utils/fp';
import { Package } from '../../../../../config/package';

interface Code extends Omit<ResourceCommon, 'urn'> {
    name: LocalizedName;
}

interface CodeList extends ResourceDescription {
    codes: Code[];
}

interface Data {
    codelists: CodeList[];
}

export interface CodeListResponse {
    data: Data;
    meta: Meta;
}

export const codeListParser = async (
    xml: Promise<any>
): Promise<CodeListResponse> => {
    const dataMessage = await xml;
    const dataStructure =
        dataMessage['message:Structure']['message:Structures'][0][
            'structure:Codelists'
        ][0]['structure:Codelist'][0];
    const meta = getMeta(dataMessage);
    const name = getLocalizedName(dataStructure);
    const { agencyID, id, isFinal, urn, version } = dataStructure.$;
    const codes = dataStructure['structure:Code'].reduce((acc, code) => {
        const { id, urn } = code['$'];
        const links = [
            {
                rel: Relation.SELF,
                type: Package.CODE,
                urn,
            },
        ];
        const name = getLocalizedName(code);
        return [...acc, { id, links, name }];
    }, []);
    const links = [
        {
            rel: Relation.SELF,
            type: Package.CODE_LIST,
            urn,
        },
    ];
    const codeList = {
        agencyID,
        id,
        isFinal: stringToBool(isFinal),
        links,
        codes,
        name,
        urn,
        version,
    };
    const data: Data = { codelists: [codeList] };
    return { meta, data };
};
