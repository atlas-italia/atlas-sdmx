import {
    buildUrn,
    getLocalizedName,
    getMeta,
} from '../../../../../utils/parser';
import {
    Link,
    Meta,
    Relation,
    ResourceDescription,
} from '../../../../../typings/structuralMetadata';
import { Package } from '../../../../../config/package';
import { stringToBool } from '../../../../../utils/fp';

interface DataFlow extends ResourceDescription {
    structure: string;
}

interface Data {
    dataflows: DataFlow[];
}

export interface DataFlowResponse {
    meta: Meta;
    data: Data;
}

export const dataflowParser = async (
    xml: Promise<any>
): Promise<DataFlowResponse> => {
    const dataMessage = await xml;
    const messageStructure = dataMessage['message:Structure'];
    const structureDataFlow = messageStructure['message:Structures'][0][
        'structure:Dataflows'
    ][0]['structure:Dataflow'] as any[];
    const meta = getMeta(dataMessage);
    const dataflows: DataFlow[] = structureDataFlow.map((df: any) => {
        const { agencyID, id, isFinal, urn, version } = df.$;
        const name = getLocalizedName(df);
        const dataStructureUrn = buildUrn(
            df['structure:Structure'][0].Ref[0].$
        );
        const selfLink: Link = {
            urn,
            rel: Relation.SELF,
            type: Package.DATA_FLOW,
        };

        const structureLink: Link = {
            urn: dataStructureUrn,
            rel: Relation.STRUCTURE,
            type: Package.DATA_STRUCTURE,
        };

        return {
            agencyID,
            id,
            isFinal: stringToBool(isFinal),
            links: [selfLink, structureLink],
            name,
            structure: dataStructureUrn,
            urn,
            version,
        };
    });
    return { data: { dataflows }, meta };
};
