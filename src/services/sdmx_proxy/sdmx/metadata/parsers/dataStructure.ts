import {
    buildUrn,
    getLocalizedName,
    getMeta,
} from '../../../../../utils/parser';
import {
    Link,
    Meta,
    Relation,
    ResourceCommon,
    ResourceDescription,
} from '../../../../../typings/structuralMetadata';
import { Package } from '../../../../../config/package';
import { stringToBool } from '../../../../../utils/fp';

interface DimensionList {
    id: string;
    dimensions: Dimension[];
    links: Link[];
    urn: string;
}

interface Dimension extends ResourceCommon {
    conceptIdentity: string;
    localRepresentation: LocalRepresentation;
    position: number;
    type: Package;
    urn: string;
}

interface LocalRepresentation {
    enumeration: string;
}

interface Data {
    dataStructures: DataStructure[];
}

interface DataStructure extends ResourceDescription {
    dataStructureComponents: DataStructureComponent;
}

interface DataStructureComponent {
    dimensionList: DimensionList;
}

export interface DataStructureResponse {
    meta: Meta;
    data: Data;
}

const getDimension = (dimensionData): Dimension => {
    const { id, urn, position } = dimensionData.$;
    return {
        id,
        conceptIdentity: buildUrn(
            dimensionData['structure:ConceptIdentity'][0].Ref[0].$
        ),
        links: [
            {
                rel: Relation.SELF,
                type: Package.DIMENSION,
                urn,
            },
        ],
        localRepresentation: {
            enumeration: buildUrn(
                dimensionData['structure:LocalRepresentation'][0][
                    'structure:Enumeration'
                ][0].Ref[0].$
            ),
        },
        position: parseInt(position, 10),
        type: Package.DIMENSION,
        urn,
    };
};

const getDimensionListAttr = ({
    $: { id, urn },
}): Omit<DimensionList, 'dimensions'> => ({
    id,
    links: [
        {
            rel: Relation.SELF,
            type: Package.DIMENSION_DESCRIPTOR,
            urn,
        },
    ],
    urn,
});

export const dataStructureParser = async (
    xml: Promise<any>
): Promise<DataStructureResponse> => {
    const dataMessage = await xml;
    const dataStructure =
        dataMessage['message:Structure']['message:Structures'][0][
            'structure:DataStructures'
        ][0]['structure:DataStructure'][0];
    const dimensionList =
        dataStructure['structure:DataStructureComponents'][0][
            'structure:DimensionList'
        ][0];
    const dataDimensions = dimensionList['structure:Dimension'];
    const name = getLocalizedName(dataStructure);
    const {
        agencyID,
        id,
        isFinal,
        urn,
        validFrom,
        validTo,
        version,
    } = dataStructure.$;
    const links: Link[] = [
        {
            rel: Relation.SELF,
            type: Package.DATA_STRUCTURE,
            urn,
        },
    ];
    const meta = getMeta(dataMessage);
    const dimensions = dataDimensions.map(getDimension);
    const dataStructureComponents: DataStructureComponent = {
        dimensionList: {
            ...getDimensionListAttr(dimensionList),
            dimensions,
        },
    };
    const data: Data = {
        dataStructures: [
            {
                agencyID,
                dataStructureComponents,
                id,
                isFinal: stringToBool(isFinal),
                links,
                name,
                urn,
                validFrom,
                validTo,
                version,
            },
        ],
    };
    return { meta, data };
};
