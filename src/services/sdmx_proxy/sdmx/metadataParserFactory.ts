import { dataflowParser } from './metadata/parsers/dataFlow';
import { dataStructureParser } from './metadata/parsers/dataStructure';
import { codeListParser } from './metadata/parsers/codeList';
import { parseXml } from './xml/xmlparser';
import { pipe } from 'ramda';
import { MetaDataResponse } from '../../../typings/structuralMetadata';
import { constraintParser } from './metadata/parsers/availableConstraint';
import { Package } from '../../../config/package';

export interface MetadataParserFactory {
    createParser: <T extends MetaDataResponse>(
        resource: Package
    ) => (x: any) => Promise<T>;
}

const FACTORIES: Record<Package, any> = {
    [Package.AVAILABLE_CONSTRAINT]: constraintParser,
    [Package.CODE]: '',
    [Package.CODE_LIST]: codeListParser,
    [Package.CONCEPT_SCHEME]: '',
    [Package.DATA_FLOW]: dataflowParser,
    [Package.DATA_STRUCTURE]: dataStructureParser,
    [Package.DIMENSION]: '',
    [Package.DIMENSION_DESCRIPTOR]: '',
};

const createParser = <T extends MetaDataResponse>(resource: Package) =>
    pipe<any, Promise<any>, Promise<T>>(parseXml, FACTORIES[resource]);

export const parserFactory = { createParser };
