import {
    availableConstraintExpected,
    availableConstraintXml,
    codeListExpected,
    codeListXml,
    dataFlowExpected,
    dataFlowXml,
    dataStructureExpected,
    dataStructureXml,
} from './__tests__/fixtures/metadata';
import { constraintParser } from './metadata/parsers/availableConstraint';
import { parseXml } from './xml/xmlparser';
import { codeListParser } from './metadata/parsers/codeList';
import { dataStructureParser } from './metadata/parsers/dataStructure';
import { dataflowParser } from './metadata/parsers/dataFlow';

describe('Metadata Parser ', () => {
    describe('availableConstraintParser', () => {
        test('should correctly parse the constraint response', async () => {
            const xml = parseXml(availableConstraintXml);
            const result = await constraintParser(xml);
            expect(result).toStrictEqual(availableConstraintExpected);
        });
    });

    describe('dataflowParser', () => {
        test('should correctly parse the dataflow response', async () => {
            const xml = parseXml(dataFlowXml);
            const result = await dataflowParser(xml);
            expect(result).toStrictEqual(dataFlowExpected);
        });
    });

    describe('dataStructureParser', () => {
        test('should correctly parse the data structure', async () => {
            const xml = parseXml(dataStructureXml);
            const result = await dataStructureParser(xml);
            expect(result).toStrictEqual(dataStructureExpected);
        });
    });

    describe('codeListParser', () => {
        test('should correctly parse the data structure', async () => {
            const xml = parseXml(codeListXml);
            const result = await codeListParser(xml);
            expect(result).toStrictEqual(codeListExpected);
        });
    });
});
