import * as xml2js from 'xml2js';

export const parseXml = (xml: any) => xml2js.parseStringPromise(xml);
