import { URL } from 'url';
import { ApiVersion } from '../../config/api';
import { AgencyID } from '../../config/agency';
import {
    DataRequest,
    MetadataQuery,
    MetadataQueryWithResource,
    MetaDataResponse,
} from '../../typings/structuralMetadata';
import { requestService, RequestService } from '../../api/request';
import { identity, times } from 'ramda';
import {
    MetadataParserFactory,
    parserFactory,
} from './sdmx/metadataParserFactory';

export interface SDMXService {
    getAvailableConstraint(params: MetadataQuery): Promise<any>;

    getCodeList(params: MetadataQuery): Promise<MetaDataResponse>;

    getData(params: DataRequest): Promise<MetaDataResponse>;

    getDataFlow(params?: MetadataQuery): Promise<MetaDataResponse>;

    getDataStructure(params: MetadataQuery): Promise<MetaDataResponse>;
}

export interface AgencyConfig {
    agencyID: AgencyID;
    api?: ApiVersion;
    id: string;
    ip?: string;
    name: string;
    restUrl: string;
}

export abstract class AbstractStatProxyFactory implements SDMXService {
    protected readonly config: AgencyConfig;

    public constructor(
        protected readonly factory: MetadataParserFactory = parserFactory,
        protected readonly request: RequestService = requestService
    ) {
        this.config = {
            ...this.getConfig(),
            api: ApiVersion.LATEST,
        };
    }

    abstract getAvailableConstraint(params: MetadataQuery): Promise<any>;

    abstract getCodeList(params: MetadataQuery): Promise<MetaDataResponse>;

    abstract getData(params: DataRequest): Promise<MetaDataResponse>;

    abstract getDataFlow(params?: MetadataQuery): Promise<MetaDataResponse>;

    abstract getDataStructure(params: MetadataQuery): Promise<MetaDataResponse>;

    protected abstract getConfig(): AgencyConfig;

    protected createDataRequestKey = (
        places: number,
        values: Record<number, string>
    ) => times(n => values[n] || '.', places);

    protected requestMetaDataFromJsonStructure<T extends MetaDataResponse>(
        requestParams: MetadataQueryWithResource
    ): Promise<T> {
        const {
            itemID,
            format = 'jsonstructure',
            resourceID,
            resource,
            version,
        } = requestParams;
        const queryParams = { format };
        const agencyID = requestParams.agencyID ?? this.config.agencyID;
        const request = [resource, agencyID, resourceID, version, itemID]
            .filter(Boolean)
            .join('/');
        const url = new URL(`${this.config.restUrl}/${request}`);
        Object.keys(queryParams || {}).forEach((key: string) => {
            queryParams[key] && url.searchParams.append(key, queryParams[key]);
        });
        return this.request<T>(`${url}`);
    }

    protected requestMetaDataFromXMLFormat<T extends MetaDataResponse>(
        requestParams: MetadataQueryWithResource
    ): Promise<T> {
        const {
            detail,
            itemID,
            references,
            resource,
            resourceID,
            version,
        } = requestParams;
        const agencyID = requestParams.agencyID ?? this.config.agencyID;
        const request = [resource, agencyID, resourceID, version, itemID]
            .filter(Boolean)
            .join('/');
        const url = new URL(`${this.config.restUrl}/${request}`);
        Object.keys({ detail, references }).forEach(
            (key: string) =>
                requestParams[key] &&
                url.searchParams.append(key, requestParams[key])
        );

        const parser = this.factory.createParser<T>(resource);
        return this.request<string>(`${url}`).then(parser).then(identity);
    }
}
