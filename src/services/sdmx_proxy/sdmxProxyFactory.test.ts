import { Agency } from '../../config/agency';
import { createService } from './sdmxProxyFactory';
import { Package } from '../../config/package';
import { SDMXService } from './sdmxProxyAbstract';
import { MetadataParserFactory } from './sdmx/metadataParserFactory';

describe('sdmxProxyFactory', () => {
    let service: SDMXService;
    const reqeustMock = jest.fn();
    const parserMock = jest.fn();
    const createParser = () => parserMock;

    const getFirstArgument = ({ mock }) => mock.calls[0][0];

    beforeEach(() => {
        service = createService(
            Agency.ISTAT,
            {
                createParser,
            } as MetadataParserFactory,
            reqeustMock
        );
    });

    afterEach(() => {
        reqeustMock.mockRestore();
        parserMock.mockRestore();
    });

    describe('getAvailableConstraint', () => {
        beforeEach(() => {
            reqeustMock.mockResolvedValueOnce('A response');
            service.getAvailableConstraint({
                resourceID: '41_983',
            });
        });

        test('makes the request with the correct given params', async () => {
            expect(reqeustMock).toHaveBeenCalledTimes(1);
            expect(getFirstArgument(reqeustMock)).toContain(
                `${Package.AVAILABLE_CONSTRAINT}/41_983`
            );
        });

        test('makes the request with the correct given params', async () => {
            expect(parserMock).toHaveBeenNthCalledWith(1, 'A response');
        });
    });
});
