import { Agency } from '../../config/agency';
import { MetadataParserFactory } from './sdmx/metadataParserFactory';
import { RequestService } from '../../api/request';
import { IstatProxyStatService } from './IstatProxyStatService';
import { SDMXService } from './sdmxProxyAbstract';

const FACTORIES: Record<
    Agency,
    (
        factory?: MetadataParserFactory,
        makeRequest?: RequestService
    ) => SDMXService
> = {
    [Agency.ISTAT]: (factory, makeRequest) =>
        new IstatProxyStatService(factory, makeRequest),
};

export const createService = (
    statAgency: Agency,
    factory?: MetadataParserFactory,
    makeRequest?: RequestService
): SDMXService => FACTORIES[statAgency](factory, makeRequest);
