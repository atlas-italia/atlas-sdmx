import { Locale } from './structuralMetadata';

export interface Dependencies {
    currentLocale: Locale;
}
