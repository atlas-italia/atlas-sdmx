export type Callback<Args extends any[]> = (...params: Args) => void;
export type Dictionary<T> = { [x: string]: T };
export type Maybe<T> = T | void | null | undefined;
export type Reject<T> = (reason?: any) => void;
export type Resolve<T> = (value?: T | PromiseLike<T>) => void;
export type Tuple<T, K> = [Array<T>, Array<K>];
