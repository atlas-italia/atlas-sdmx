import { Maybe } from '.';
import { AgencyID } from '../config/agency';
import { Package } from '../config/package';

type Key = Array<string | number | '.'>;

enum Detail {}

enum MetaDataReference {}

export enum DataResponseFormat {
    JSON = 'application/json',
    SDMX = 'application/vnd.sdmx.genericdata+xml;version=2.1',
    SDMX_CSV = 'application/vnd.sdmx.data+csv;version=1.0.0',
    SDMX_JSON = 'application/vnd.sdmx.data+json;version=1.0.0',
}

export interface Meta {
    id: string;
    prepared: string;
    receiverID: Maybe<string>;
    senderID: Maybe<string>;
    test: boolean;
    schema: Maybe<string>;
}

export type LocalizedName = Record<Locale, string>;

export enum Locale {
    EN = 'en',
    IT = 'it',
}

export enum Relation {
    SELF = 'self',
    STRUCTURE = 'structure',
}

export interface Link {
    rel: Relation;
    type: Package;
    urn: string;
}

export interface MetaDataResponse {
    meta: Meta;
    data: any;
}

export interface Ref
    extends Pick<ResourceDescription, 'agencyID' | 'id' | 'version'> {
    maintainableParentID: string;
    maintainableParentVersion: string;
    package: Package;
    class: string;
}

export interface ResourceCommon {
    id: string;
    links: Link[];
    urn: string;
}

export interface ResourceDescription extends ResourceCommon {
    agencyID: AgencyID;
    isFinal: boolean;
    name: LocalizedName;
    // @TODO: gonna be date
    validFrom?: string;
    // @TODO: gonna be date
    validTo?: string;
    version?: string;
}

interface DataQueryParams {
    // (inclusive). ISO8601 (e.g. 2014-01) or SDMX reporting period (e.g. 2014-Q3).
    startPeriod: string;
    // (inclusive). ISO8601 (e.g. 2014-01) or SDMX reporting period (e.g. 2014-Q3).
    endPeriod: string;
    updatedAfter: string;
    detail: Detail;
    firstNObservations?: number;
    lastNObservations?: number;
}

export interface DataRequest {
    flowRef: string;
    key: Key;
    queryParams?: Partial<DataQueryParams>;
    resource?: DataResource;
    resourceID?: string;
}

export enum DataResource {
    DATA = 'data',
    METADATA = 'metadata',
}

export interface MetadataQueryWithResource extends MetadataQuery {
    resource: Package;
}

export interface MetadataQuery {
    agencyID?: AgencyID;
    detail?: Detail;
    itemID?: string;
    format?: 'jsonstructure' | '';
    references?: MetaDataReference;
    resourceID?: string;
    version?: string;
}
