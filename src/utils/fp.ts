import { tap } from 'ramda';

export const stringToBool = (value: string) => value === 'true';

// eslint-disable-next-line no-console, no-sequences
export const logger = tap(console.log.bind(console));
