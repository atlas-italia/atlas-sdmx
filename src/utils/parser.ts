import { stringToBool } from './fp';
import {
    Locale,
    LocalizedName,
    Meta,
    Ref,
} from '../typings/structuralMetadata';

const BASE_URN_INFO_MODEL = 'urn:sdmx:org.sdmx.infomodel';

export const getMeta = (dataMessage: any): Meta => {
    const messageStructure = dataMessage['message:Structure'];
    const messageHeader = messageStructure['message:Header'][0];
    return <Meta>{
        id: messageHeader['message:ID'][0],
        prepared: messageHeader['message:Prepared'][0],
        receiverID: messageHeader['message:Receiver'][0].$.id,
        schema: null,
        senderID: messageHeader['message:Sender'][0].$.id,
        test: stringToBool(messageHeader['message:Test'][0]),
    };
};

export const getLocalizedName = (name: any): LocalizedName => ({
    [Locale.EN]: '',
    [Locale.IT]: '',
    ...name['common:Name'].reduce(
        (acc: any, commonName: any) => ({
            ...acc,
            [commonName.$['xml:lang']]: commonName._,
        }),
        {}
    ),
});

export const buildUrn = ({
    id,
    maintainableParentID,
    maintainableParentVersion,
    version,
    agencyID,
    package: packageName,
    class: className,
}: Ref): string => {
    if (maintainableParentID) {
        return `${BASE_URN_INFO_MODEL}.${packageName}.${className}=${agencyID}:${maintainableParentID}(${maintainableParentVersion}).${id}`;
    }
    return `${BASE_URN_INFO_MODEL}.${packageName}.${className}=${agencyID}:${id}(${version})`;
};
