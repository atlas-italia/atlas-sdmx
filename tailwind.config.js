module.exports = {
    purge: [],
    darkMode: false,
    theme: {
        extend: {
            colors: {
                blue: {
                    'atlas-dark': '#1E2F53',
                    'atlas-darker': '#061025',
                    'atlas-light': '#65AEF8',
                    500: '#4A95E1',
                },
                pink: {
                    atlas: '#C885DE',
                },
            },
        },
    },
    variants: {},
    plugins: [],
};
