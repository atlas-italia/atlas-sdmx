provider "google" {
  credentials = "${path.module}/../.gcp/atlas-sdmx-credentials.json"
  project = "atlas-sdmx"
  region = "europe-west3"
  zone = "europe-west3-a"
}

data "archive_file" "atlas_sdmx" {
  type = "zip"
  output_path = "${path.module}/../artifacts/atlas_sdmx.zip"

  source_dir = "${path.module}/../dist/functions/"
}

resource "google_storage_bucket" "atlas_sdmx_ci" {
  provider = google
  name = "atlas-sdmx-ci"
  location = "EU"

  force_destroy = true

  depends_on = [
    data.archive_file.atlas_sdmx]
}

resource "google_storage_bucket_object" "data_flow_archive_zip" {
  provider = google
  name = "atlas_sdmx.zip"
  bucket = google_storage_bucket.atlas_sdmx_ci.name

  source = data.archive_file.atlas_sdmx.output_path

  depends_on = [
    data.archive_file.atlas_sdmx]
}

resource "google_cloudfunctions_function" "atlas_sdmx_api" {
  provider = google
  name = "atlas-sdmx-api"
  description = "[Managed by Terraform] Atlas SDMX set of utilities"
  runtime = "nodejs12"
  timeout = 500

  source_archive_bucket = google_storage_bucket.atlas_sdmx_ci.name
  source_archive_object = google_storage_bucket_object.data_flow_archive_zip.name
  trigger_http = true
  entry_point = "app"

  depends_on = [
    google_storage_bucket_object.data_flow_archive_zip]
}

resource "google_cloudfunctions_function_iam_member" "invoker" {
  provider = google
  region = google_cloudfunctions_function.atlas_sdmx_api.region
  cloud_function = google_cloudfunctions_function.atlas_sdmx_api.name

  role = "roles/cloudfunctions.invoker"
  member = "allUsers"
}
