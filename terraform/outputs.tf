output "atlas_sdmx_https_trigger_url" {
  value = google_cloudfunctions_function.atlas_sdmx_api.https_trigger_url
}
