const HtmlPlugin = require('html-webpack-plugin');
const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const webpack = require('webpack');

const scriptsPath = path.resolve(__dirname, './');
const srcBasePath = `${scriptsPath}/src`;
const destPath = `${scriptsPath}/public`;

if (process.env.WEBPACK_DEV_SERVER) {
    require('dotenv').config();
}

module.exports = {
    devServer: {
        contentBase: destPath,
        historyApiFallback: true,
        hot: true,
        inline: true,
        port: 9000,
        watchOptions: {
            ignored: /node_modules/,
        },
    },
    entry: {
        main: `${srcBasePath}/index.ts`,
    },
    performance: { hints: false },
    module: {
        rules: [
            {
                test: /\.vuex?$/,
                loader: 'vue-loader',
            },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
                options: {
                    appendTsSuffixTo: [/\.vuex?$/],
                },
            },
            {
                test: /\.(s*)[a|c]ss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    'vue-style-loader',
                    // Translates CSS into CommonJS
                    {
                        loader: 'css-loader',
                        options: { importLoaders: 1 },
                    },
                    // Compiles Sass to CSS
                    {
                        loader: 'sass-loader',
                        options: {
                            sassOptions: {
                                includePaths: ['node_modules'],
                            },
                        },
                    },
                    'postcss-loader',
                ],
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: ['file-loader'],
            },
        ],
    },
    output: {
        path: destPath,
        filename: '[name][hash].js',
    },
    plugins: [
        new HtmlPlugin({
            template: `${srcBasePath}/index.html`,
        }),
        new VueLoaderPlugin(),
        new webpack.EnvironmentPlugin({
            ATLAS_SDMX_HOST: '',
            ATLAS_SDMX_PORT: '',
        }),
    ],
    resolve: {
        extensions: ['*', '.ts', '.js', '.vue', '.json', '.mjs'],
        alias: {
            vue: 'vue/dist/vue.esm-bundler.js',
        },
    },
};
