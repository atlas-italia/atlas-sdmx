const nodeExternals = require('webpack-node-externals');
const path = require('path');
const webpack = require('webpack');

const scriptsPath = path.resolve(__dirname, './');
const functionsBasePath = `${scriptsPath}/src/functions`;
const destPath = `${scriptsPath}/dist/functions`;

module.exports = {
    entry: `${functionsBasePath}/index.ts`,
    externals: [nodeExternals()],
    mode: 'production',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    target: 'node',
    optimization: {
        usedExports: true,
    },
    output: {
        path: destPath,
        filename: 'index.js',
        libraryTarget: 'this',
    },
    resolve: {
        extensions: ['*', '.ts', '.js', '.vue', '.json', '.mjs'],
    },
};
